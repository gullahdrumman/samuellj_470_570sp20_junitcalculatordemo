package edu.uscb.csci470.calculator.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.*;
// import org.junit.Test;

import edu.uscb.csci470.calculator.Calculator;

// NOTE: make sure your pom.xml file has the necessary dependencies
//   so that Eclipse knows what to import!

public class CalculatorTest {
   
    private Calculator calculatorTestInstance; // has 
    private static Logger logger = LoggerFactory.getLogger(CalculatorTest.class );
    
    @BeforeClass
    public static void setupBeforeAllTest() {
    	logger.info( "Startingg test for this class...");
    }
    
    @Before
    public void setUpBeforeEachTest() {
	
    	calculatorTestInstance = new Calculator();
    	logger.info(" new test instance" + calculatorTestInstance);
    }

    @After
    public void teardownAfterEachTest() {
    	logger.info( "...a test was just run ...");
    } // end method teardownAfterEachTest()
    
    @AfterClass
    public static void teardownAfterAllTests() {
        logger.info("A'' tests completed, closing DB connections etc." );	
    } // end method
    
    //
    @Test
    public void testAdd() {
    	int value1 = 25;
    	int value2 = 10;
    	int expectedResult = 35;
    	int testResult = calculatorTestInstance.add( value1, value2);
    	Assert.assertEquals( expectedResult, testResult);
     // end test method testAdd
    }
    
    @Test
    public void testSubtract() {
    	int value1 = 25;
    	int value2 = 10;
    	int expectedResult = 15;
    	int testResult = calculatorTestInstance.subtract( value1, value2);
    	Assert.assertEquals( expectedResult, testResult);
     // end test method testSubtract
    }
    
    @Test
    public void testMultiplication() {
    	int value1 = 25;
    	int value2 = 10;
    	long expectedResult = 250;
    	long testResult = calculatorTestInstance.multiply( value1, value2);
    	Assert.assertEquals( expectedResult, testResult);
     // end test method testMultiplication
    }
     
    @Test
    public void testIntDivision() {
    	int value1 = 30;
    	int value2 = 10;
    	int expectedResult = 3;
    	int testResult = calculatorTestInstance.intDivide( value1, value2);
    	Assert.assertEquals( expectedResult, testResult);
    } // end test method testDivision
    
    @Test( expected = IllegalArgumentException.class)
    public void testIntDivideByZero() {
    	int value1 = 25;
    	int value2 = 0;
    	try {
    		calculatorTestInstance.intDivide(value1, value2);
    	} 
    	finally {
    		System.out.println("testIntDivideByZero complete");
    	} // end try statement
        calculatorTestInstance.intDivide(value1, value2);
    } // end method testIntDivideByZero
    
} // end class CalculatorTest