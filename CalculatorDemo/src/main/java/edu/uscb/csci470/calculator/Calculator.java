package edu.uscb.csci470.calculator;

/**
 * Calculator class that implements basic 
 * arithmetic operations as named methods
 * 
 * @author your_username@email.uscb.edu
 * @version 0.1
 */
public class Calculator {

	public int add( int a, int b ) {
		return a + b;
	}
	public int subtract( int a, int b ) {
		return a - b;
	}
	public long multiply( int a, int b ) {
		return a * b;
	}
	public int intDivide( int a, int b ) {
		int result;
		if (b == 0) {
			throw new IllegalArgumentException(
					"intDivide method: Cannot divide by zero");
		} else {
			result = a / b;
		} // end if/else
		return result;
	} // end method intDivide
		
		
		

} // end class Calculator
